# example

demo project building an alpine image with nano installed

### in gitlab

create new repo with README.md

tag repo with 0.0.1

generate a personal access token
  1. open [your gitlab profile](https://gitlab.com/profile)
  1. click Access Tokens
  1. enter a name
  1. tick the API checkbox
  1. click Create personal access token
  1. copy the access token (do not commit this anywhere)

add variables in gitlab -> settings -> ci_cd -> Variables
  * GITLAB_AUTH_TOKEN - set to your personal access token, PROTECTED

__GITLAB_AUTH_TOKEN variable can be added to the project OR group__


### on your machine

  1. clone your new repo
  1. copy files from this repo
  1. modify Dockerfile to build your image
  1. commit your change with a [semantic commit message](https://github.com/angular/angular.js/blob/master/DEVELOPERS.md#-git-commit-guidelines)


### automation using gitlab ci
when you merge your change to master:
  1. master branch will be tagged with a [semantic version number](https://semver.org/)
  1. docker image will be build and pushed to:
    * my-docker-image:latest
    * my-docker-image:version
  1. a gitlab.io page is generated and published with:
    * your docker image name and version
    * build date
    * package list
